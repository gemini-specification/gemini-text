
[NOTE:  This is plain text file for maximum readability.  Gemini specifies
its own text format which gitlab does not support.  While the use of HTML or
Markdown could be used, the preference here is not to use such formats as
they are against the grain of what Gemini is trying to achieve.  That leaves
plain text for now.]

This repository is to revise and finalize the Gemini text specification
only.  There is a second repository for the revision and finalization of the
Gemini protocol format: https://gitlab.com/gemini-specification/protocol


